from sys import stdin
import curses
import time

frameBorder = '```'


def readFrames():
    frames = []
    data = stdin.readlines()
    isFrameData = False
    frame = []
    for dataLine in data:
        line = dataLine.rstrip('\n')
        if line == frameBorder:
            isFrameData = not isFrameData
            if frame:
                frames.append(frame)
                frame = []
            continue
        if isFrameData:
            frame.append(line)
    return frames


def printFrames(stdscr):
    frames = readFrames()
    stdscr.clear()
    stdscr.refresh()

    curses.start_color()
    curses.init_pair(1, curses.COLOR_CYAN, curses.COLOR_BLACK)
    frameIndex = 0

    while True:
        y = 0
        frame = frames[frameIndex]
        for line in frame:
            stdscr.addstr(y, 0, line, curses.color_pair(1))
            y += 1
        stdscr.move(0, 0)
        stdscr.refresh()
        time.sleep(0.2)
        stdscr.clear()
        frameIndex += 1
        if frameIndex >= len(frames):
            frameIndex = 0


curses.wrapper(printFrames)
curses.endwin()